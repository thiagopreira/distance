package com.olx.distance.api;

import static org.junit.Assert.assertEquals;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.junit.Test;

public class DistanceCalculatorTest {

	private static final String WORD_A = "banana";
	private static final String WORD_B = "abacate";
	
	
	@Test
	public void testBothEmptyString() {
		int distance = DistanceCalculator.distance(null, null);
		assertEquals(distance, 0);
		distance = DistanceCalculator.distance(EMPTY, null);
		assertEquals(distance, 0);
	}

	@Test
	public void testOneEmptyString() {		
		int distance = DistanceCalculator.distance(WORD_A, EMPTY);
		assertEquals(distance,WORD_A.length());
		distance = DistanceCalculator.distance(EMPTY, WORD_B);
		assertEquals(distance,WORD_B.length());
	}	
	
	@Test
	public void testDistanceEqualWords(){
		int distance = DistanceCalculator.distance(WORD_A, WORD_A);
		assertEquals(distance,0);
	}
	
	@Test
	public void testDistanceWordAAndWordB(){
		int distance = DistanceCalculator.distance(WORD_A, WORD_B);
		assertEquals(distance,4);
	}

}
