package com.olx.distance.api;

import static com.olx.distance.api.Similar.areSimilar;
import static org.junit.Assert.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import org.junit.Test;


public class SimilarTest {
	
	private static final String WORD_A = "banana";
	private static final String WORD_B = "abacate";

	
	@Test
	public void testSimilarEmptyWords(){
		boolean similar = areSimilar(EMPTY, EMPTY, 3);
		assertTrue(similar);
		similar = areSimilar(WORD_A, EMPTY, 3);
		assertFalse(similar);
	}
	
	@Test
	public void testSimilarWordsWithNoThreshold(){
		boolean similar = areSimilar(WORD_A, WORD_B, 0);
		assertFalse(similar);
	}
	
	@Test
	public void testSimilarWordsWithThreshold(){
		boolean similar = areSimilar(WORD_A, WORD_B, 4);
		assertTrue(similar);
	}
}
