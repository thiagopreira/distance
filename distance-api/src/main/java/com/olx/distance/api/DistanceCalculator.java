package com.olx.distance.api;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.math.NumberUtils.min;

public abstract class DistanceCalculator {

	public static int distance(String wordA, String wordB) {
		
		//empty words
		if (isEmpty(wordA) && isEmpty(wordB)) {
			return 0;
		}
		
		if (isEmpty(wordA)) {
			return wordB.length();
		}

		if (isEmpty(wordB)) {
			return wordA.length();
		}
		
		//same words
		if(wordA.equals(wordB)){
			return 0;
		}
		
		//initialize distance matrix
		int[][] distance = new int[wordA.length() + 1][wordB.length() + 1];

		for (int i = 0; i <= wordA.length(); i++)
			distance[i][0] = i;
		for (int j = 1; j <= wordB.length(); j++)
			distance[0][j] = j;

		//calculate distance
		for (int i = 1; i <= wordA.length(); i++)
			for (int j = 1; j <= wordB.length(); j++){
				//if char are equals in both strings, then matches
				int match	 = ((wordA.charAt(i - 1) == wordB.charAt(j - 1)) ? 0 : 1);
				
				int delete = distance[i - 1][j] + 1;
				int insert = distance[i][j - 1] + 1;
				int replace = distance[i - 1][j - 1] + match;
				
				//minimum of 3 options is inputed on the matrix
				distance[i][j] = minimum(delete,insert ,replace);
			}
		
		//last position on matrix is the response
		return distance[wordA.length()][wordB.length()];
	}


	private static int minimum(int delete, int insert, int replace) {
		return min(delete, insert, replace);
	}

}
