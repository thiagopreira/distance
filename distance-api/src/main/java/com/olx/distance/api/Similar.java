package com.olx.distance.api;

import static com.olx.distance.api.DistanceCalculator.distance;

public abstract class Similar {
	
	private static final int DEFAULT_THRESHOLD=3;

	public static boolean areSimilar(String wordA, String wordB, int threshold){	
		if(threshold==0){
			threshold=DEFAULT_THRESHOLD;
		}
		if(distance(wordA, wordB) <= threshold)
			return true;		
		return false;
	}
}
