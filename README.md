#Desafio OLX

Projeto resultado do desafio **Distance Challenge**.
Neste repositório se encontra um projeto maven com 3 módulos.

* *distance-api* : implementação da regra de negócio proposta pelo desafio
* *word-persistence* : projeto Spring Boot de persistencia utilizando o banco em memória H2
* *word-web* : projeto Spring Boot com os controladores e serviços responsáveis pela disponibilização da API Rest

##Utilização do projeto
Requisitos do projeto:

* Maven
* Java 1.8

Para executar o projeto é preciso baixar o codigo do reporistório, entrar no diretório do projeto principal (distance) e executar o seguinte comando maven:

```
mvn clean install
```

Este comando ira baixar as dependencias e executar os testes.
Após a execução do comando, ir ate do destino do projeto web (word-web) e executar o seguinte comando:

```
cd word-web
mvn spring-boot:run
```
O projeto será iniciado e estará executando na porta 8080.

##API
Este projeto conta com a interface de documentação [Swagger](http://localhost:8080/swagger-ui.htm) , que expõe uma interface onde os requests podem ser executados.
Abaixo seguem os exemplos de chamada utilizando curl:
### Save Word
```
curl -X POST
--header 'Content-Type: application/json'
--header 'Accept: application/json'
-d 'banana'
'http://localhost:8080/api/v1/words'
```

### Save Words
```
curl -X POST
--header 'Content-Type: application/json'
--header 'Accept: application/json'
-d '["abacate","abacaxi"]'
'http://localhost:8080/api/v1/words/list'
```

### List words
```
curl -X GET
--header 'Accept: application/json'
'http://localhost:8080/api/v1/words'
```

Existe a possibildade de ordenacao e paginacao das palavras

```
curl -X GET
--header 'Accept: application/json'
'http://localhost:8080/api/v1/words?page=0&size=10000&order=desc'
```

### Similar words
```
curl -X GET
--header 'Accept: application/json'
'http://localhost:8080/api/v1/words/similar/batata?threshold=3'
```
O parâmetro threshold é opcional