package com.olx.persistence.repository;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.olx.persistence.entity.Word;

@Repository
public interface WordRepository extends JpaRepository<Word, Long> {
	Word findAllByContent(String content);
	
	Page<Word> findAll(Pageable pageable);
}
