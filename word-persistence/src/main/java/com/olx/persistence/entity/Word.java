package com.olx.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Word {

	//JPA
	Word() {
	}
	
	public Word(String content) {
		this.content = content;
	}

	public Word(Long id, String content) {
		this.id = id;
		this.content = content;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String content;

}
