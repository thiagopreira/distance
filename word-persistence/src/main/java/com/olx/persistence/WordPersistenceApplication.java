package com.olx.persistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WordPersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WordPersistenceApplication.class, args);
	}


}
