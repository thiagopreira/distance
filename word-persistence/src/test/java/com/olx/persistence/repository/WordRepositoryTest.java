package com.olx.persistence.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.olx.persistence.entity.Word;


@RunWith(SpringRunner.class)
@SpringBootTest
public class WordRepositoryTest {

	@Autowired
	private WordRepository wordRepository;
	
	private Word wordA;
	private Word wordB;
	private Word wordC;
	
	@Before
	public void init(){
		wordA = new Word("banana");
		wordB = new Word("abacate");
		wordC = new Word("abacaxi");
		
		wordRepository.deleteAll();
		
		wordRepository.save(Arrays.asList(wordA,wordB,wordC));
	}
	
	@Test
	public void testSaveWord(){
		Word savedWord = new Word("laranja"); 
		wordRepository.save(savedWord);
		assertNotNull("Word is null",savedWord);
		assertNotNull(savedWord.getId());
		assertEquals(4, wordRepository.count());
	}
	
	@Test
	public void testDeleteWord(){
		wordRepository.delete(wordB);
		Word findOne = wordRepository.findAllByContent(wordB.getContent());
		assertNull(findOne);
		assertEquals(2, wordRepository.count());
	}
	
	
	
	
}
