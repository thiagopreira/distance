package com.olx.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.olx.service.WordService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WordControllerTest {

	@Autowired
	private MockMvc mockMvc;

	private static final String PATH = "/api/v1/words";
	private static final String PATH_LIST = "/api/v1/words/list";
	private static final String PATH_SIMILAR = "/api/v1/words/similar/{word}";

	private static final List<String> LIST = Arrays.asList("banana", "abacate", "abacaxi", "tomate");

	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private WordService wordService;

	@Test
	public void testSaveOneWord() throws Exception {
		mockMvc.perform(post(buildDefaultPath()).contentType(MediaType.APPLICATION_JSON).content("banana"))
				.andDo(print()).andExpect(status().isCreated()).andExpect(content().string(containsString("banana")));
	}

	@Test
	public void testSaveWords() throws Exception {
		byte[] bytes = mapper.writerWithDefaultPrettyPrinter().writeValueAsBytes(LIST);
		mockMvc.perform(post(buildSaveWordsPath()).contentType(MediaType.APPLICATION_JSON).content(bytes))
				.andDo(print()).andExpect(status().isCreated())
				.andExpect(content().string(containsString(LIST.get(0))));
	}

	@Test
	public void testListWords() throws Exception {
		mockMvc.perform(get(buildDefaultPath()).contentType(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk()).andExpect(content().string(containsString(LIST.get(0))));
	}

	@Test
	public void testListWordsPageable() throws Exception {
		wordService.save(LIST);
		Optional<String> firstWord = LIST.stream().sorted((w1, w2) -> w1.compareTo(w2)).findFirst();
		mockMvc.perform(get(buildPageablePath()).contentType(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk()).andExpect(content().string(containsString(firstWord.get())));
	}

	@Test
	public void testSimilarWords() throws Exception {
		wordService.save(LIST);
		String word = "batata";
		mockMvc.perform(get(buildSimilarWordPath(word,3)).contentType(MediaType.APPLICATION_JSON)).andDo(print())
		.andExpect(status().isOk()).andExpect(content().string(containsString("banana")));
		
		mockMvc.perform(get(buildSimilarWordPath("melao",3)).contentType(MediaType.APPLICATION_JSON)).andDo(print())
		.andExpect(status().isOk()).andExpect(content().string(containsString("")));

	}

	private String buildDefaultPath() {
		return UriComponentsBuilder.newInstance().scheme("http").host("localhost:8080").path(PATH).build()
				.toUriString();
	}

	private String buildSaveWordsPath() {
		return UriComponentsBuilder.newInstance().scheme("http").host("localhost:8080").path(PATH_LIST).build()
				.toUriString();
	}

	private String buildPageablePath() {
		return UriComponentsBuilder.newInstance().scheme("http").host("localhost:8080").path(PATH)
				.query("order={order}").query("page={page}").query("size={size}").buildAndExpand("asc", 0, 1)
				.toUriString();
	}

	private String buildSimilarWordPath(String word,int threshold) {
		return UriComponentsBuilder.newInstance().scheme("http").host("localhost:8080").path(PATH_SIMILAR)
				.query("threshold={threshold}").buildAndExpand(word,threshold).toUriString();
	}

}
