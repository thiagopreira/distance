package com.olx.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringRunner;

import com.olx.persistence.entity.Word;
import com.olx.persistence.repository.WordRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WordServiceTest {

	@Autowired
	private WordService service;
	
	@Autowired
	private WordRepository wordRepository;
	
	List<String> list = Arrays.asList("banana","abacate","abacaxi","tomate");

	@Before
	public void init(){
		wordRepository.deleteAll();
	}
	
	@Test
	public void testSaveWords(){
		List<Word> savedList = service.save(list);
		assertNotNull(savedList);
		assertEquals(savedList.size(),service.listAll().size());
	}
	
	@Test
	public void testlistAll(){
		List<Word> savedList = service.save(list);
		List<String> all = service.listAll();
		assertEquals(all.size(), savedList.size());
		List<String> contents = savedList.stream().map(w -> w.getContent()).collect(Collectors.toList());
		assertEquals(all,contents);
	}
	
	@Test
	public void testListPageable(){
		List<Word> savedList = service.save(list);
		List<String> orderedList = service.listPageable(4, 0, Direction.ASC);
		assertEquals(orderedList.size(), savedList.size());
		List<String> contents = savedList.stream().map(w -> w.getContent()).collect(Collectors.toList());
		contents.sort( (w1,w2) -> w1.compareTo(w2));
		assertEquals(orderedList,contents);
		assertEquals(orderedList.get(0),contents.get(0));
	}
	
	
	@Test
	public void testSimilarWords(){
		service.save(list);
		List<String> similarWords = service.similarWords("batata", 3);
		assertEquals(similarWords.size(),2);
		assertEquals(similarWords,Arrays.asList("banana","abacate"));
	}
	
	@Test
	public void testEqualWords(){
		service.save(list);
		List<String> similarWords = service.similarWords("banana", 3);
		assertEquals(similarWords.size(),1);
		assertEquals(similarWords,Arrays.asList("banana"));
	}
	
	
	
	
	
}
