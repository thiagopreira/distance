package com.olx.controller;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.olx.persistence.entity.Word;
import com.olx.service.WordService;

@RestController
@RequestMapping("/api/v1/words")
public class WordsController {

	
	private WordService service;

	@Autowired
	public WordsController(WordService service) {
		this.service = service;
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Word save(@RequestBody String content){
		return service.save(content);
	}
	
	@PostMapping("/list")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Word> save(@RequestBody String[] content){
		return service.save(Arrays.asList(content));
	}
	
	@GetMapping
	public List<String> list(@RequestParam(required=false) Integer page,@RequestParam(required=false) Integer size, @RequestParam(required=false) String order ){
		if(page==null && StringUtils.isEmpty(order) && size == null){
			return service.listAll();
		}
		
		Direction direction = (order.toLowerCase().contains("desc")) ? Direction.DESC:  Direction.ASC;
		page = (page == null)? 0 : page;
		size = (size == null)? Integer.MAX_VALUE : size;
		
		return service.listPageable(size, page, direction);
	}
	
	
	@GetMapping("/similar/{word}")
	public List<String> similarWords(@PathVariable("word")String word, @RequestParam(required=false,defaultValue="3")Integer threshold){
		if(StringUtils.isEmpty(word)){
			return Arrays.asList("");
		}
		return service.similarWords(word, threshold);
	}
	
	
}
