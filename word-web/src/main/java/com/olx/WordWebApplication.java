package com.olx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WordWebApplication {

	 public static void main(String[] args) {
	        SpringApplication.run(WordWebApplication.class, args);
	    }
	
}
