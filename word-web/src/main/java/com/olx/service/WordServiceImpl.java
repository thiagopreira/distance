package com.olx.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.olx.distance.api.Similar;
import com.olx.persistence.entity.Word;
import com.olx.persistence.repository.WordRepository;
@Service
public class WordServiceImpl implements WordService {

	private WordRepository wordRepository;
	
	@Autowired
	public WordServiceImpl(WordRepository wordRepository) {
		this.wordRepository = wordRepository;
	}

	
	public Word save(String content) {
		return wordRepository.save(new Word(content));
	}

	public List<Word> save(List<String> words) {
		List<Word> returnList = new ArrayList<>();
		words.forEach(word ->{
			returnList.add(save(word));
		});
		return returnList;
	}

	public List<String> listAll() {
		PageRequest request = new PageRequest(0, Integer.MAX_VALUE);
		return wordRepository.findAll(request).getContent().stream().map(word -> word.getContent()).collect(Collectors.toList());
	}

	public List<String> listPageable(int size, int page, Direction order) {
		PageRequest request = new PageRequest(page, size,order,"content");
		return wordRepository.findAll(request).getContent().stream().map(word -> word.getContent()).collect(Collectors.toList());
	}

	public List<String> similarWords(String word, int threshold) {
		List<String> all = wordRepository.findAll().stream().map(w -> w.getContent()).collect(Collectors.toList());
		return all.stream().filter( content -> Similar.areSimilar(word, content, threshold)).collect(Collectors.toList());
	}

}
