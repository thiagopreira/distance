package com.olx.service;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;

import com.olx.persistence.entity.Word;

public interface WordService {

	public Word save(String content);

	public List<Word> save(List<String> words);

	public List<String> listAll();

	public List<String> listPageable(int size, int page, Direction order);

	public List<String> similarWords(String word, int threshold);
	
	
}
